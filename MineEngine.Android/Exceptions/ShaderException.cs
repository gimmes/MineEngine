﻿using System;
using OpenTK.Graphics.ES20;

namespace Gimmes.MineEngine.Android {

	/// <summary>
	/// Shader exception.
	/// </summary>
	public class ShaderException : Exception {

		/// <summary>
		/// The type of the shader.
		/// </summary>
		readonly public ShaderType? ShaderType;

		/// <summary>
		/// The shader info log.
		/// </summary>
		readonly public string ShaderInfoLog;

		/// <summary>
		/// The program info log.
		/// </summary>
		readonly public string ProgramInfoLog;

		/// <summary>
		/// Initializes a new instance of the <see cref="Gimmes.MineEngine.ShaderException"/> class.
		/// </summary>
		ShaderException (string message)
			: base (message) {
			ShaderType = null;
		}

		/// <summary>
		/// Initializes a new instance of the <see cref="Gimmes.MineEngine.ShaderException"/> class.
		/// </summary>
		/// <param name="type">Type.</param>
		/// <param name="message">Message.</param> 
		public ShaderException (ShaderType? type, string message)
			: this (message) {
			ShaderType = type;
		}

		/// <summary>
		/// Initializes a new instance of the <see cref="Gimmes.MineEngine.ShaderException"/> class.
		/// </summary>
		/// <param name="type">Type.</param>
		/// <param name="message">Message.</param>
		/// <param name="args">Arguments.</param>
		public ShaderException (ShaderType? type, string message, params object[] args)
			: this (type, string.Format (message, args)) { }
		
		/// <summary>
		/// Initializes a new instance of the <see cref="Gimmes.MineEngine.ShaderException"/> class.
		/// </summary>
		/// <param name="type">Type.</param>
		/// <param name="shaderid">Shader ID.</param>
		public ShaderException (ShaderType? type, int shaderid)
			: this (type, "Check ShaderInfoLog and ProgramInfoLog") {
			ShaderInfoLog = GL.GetShaderInfoLog (shaderid);
			ProgramInfoLog = GL.GetProgramInfoLog (shaderid);
		}

		/// <summary>
		/// Initializes a new instance of the <see cref="Gimmes.MineEngine.ShaderException"/> class.
		/// </summary>
		/// <param name="type">Type.</param>
		/// <param name="shaderid">Shader ID.</param>
		/// <param name="format">Format.</param>
		/// <param name="args">Arguments.</param>
		public ShaderException (ShaderType? type, int shaderid, string format, params object[] args)
			: this (type, format, args) {
			ShaderInfoLog = GL.GetShaderInfoLog (shaderid);
			ProgramInfoLog = GL.GetProgramInfoLog (shaderid);
		}

		/// <summary>
		/// Initializes a new instance of the <see cref="Gimmes.MineEngine.ShaderException"/> class.
		/// </summary>
		/// <param name="shader">Shader.</param>
		public ShaderException (BasicShader shader)
			: this (shader.ShaderType, shader.ShaderId) {
		}

		/// <summary>
		/// Initializes a new instance of the <see cref="Gimmes.MineEngine.ShaderException"/> class.
		/// </summary>
		/// <param name="program">Program.</param>
		public ShaderException (ShaderProgram program)
			: this (null, program.ProgramId) {
		}
	}
}

