﻿using System;
using Android.App;
using OpenTK;

namespace Gimmes.MineEngine.Android {
	
	/// <summary>
	/// Mine game.
	/// </summary>
	public class MineGame : MineGameBase {

		/// <summary>
		/// The resources.
		/// </summary>
		readonly public ResourceHelper Resources;

		/// <summary>
		/// The sprite batch.
		/// </summary>
		public SpriteBatch SpriteBatch;

		/// <summary>
		/// Gets the width.
		/// </summary>
		/// <value>The width.</value>
		public override int Width { get { return View.Width; } }

		/// <summary>
		/// Gets the height.
		/// </summary>
		/// <value>The height.</value>
		public override int Height { get { return View.Height; } }

		/// <summary>
		/// Gets the <see cref="GameConfiguration"/>.
		/// </summary>
		/// <value>The game configuration.</value>
		public override GameConfiguration Configuration {
			get { return View.Configuration; }
		}

		/// <summary>
		/// Gets the touch information.
		/// </summary>
		/// <value>The touch information.</value>
		public TouchInformation TouchInformation {
			get { return View.TouchInformation; }
		}

		/// <summary>
		/// The <see cref="MineGameView"/>.
		/// </summary>
		readonly MineGameView View;

		bool PreparationDone;

		public MineGame (GameConfiguration conf, Activity context)
			: base (conf) {

			// Create game view
			View = new MineGameView (this, conf, context);

			// Create resource helper
			Resources = new ResourceHelper (context);

			// Register main callbacks
			View.RegisterUpdateCallback (InternalUpdate);
			View.RegisterUpdateCallback (OnUpdate);
			View.RegisterRenderCallback (OnRender);

			// Set content view
			context.SetContentView (View);
		}

		/// <summary>
		/// Run the game.
		/// </summary>
		public override void Run () {
			View.Run (View.Configuration.TargetFPS);
		}

		/// <summary>
		/// Pause the game.
		/// </summary>
		public override void Pause () {
			View.Pause ();
		}

		/// <summary>
		/// Resume the game.
		/// </summary>
		public override void Resume () {
			View.Resume ();
		}

		/// <summary>
		/// Swaps the buffers.
		/// </summary>
		public void SwapBuffers () {
			View.SwapBuffers ();
		}

		/// <summary>
		/// Happens after the initialization of the GLES context.
		/// </summary>
		public override void Prepare () {
			
			// Create sprite batch
			SpriteBatch = new SpriteBatch (this);
		}

		/// <summary>
		/// Happens before rendering.
		/// </summary>
		/// <param name="e">Event arguments.</param>
		public override void OnUpdate (UpdateEventArgs e) {
		}

		/// <summary>
		/// Happens after updating.
		/// </summary>
		/// <param name="e">Event arguments.</param>
		public override void OnRender (UpdateEventArgs e) {
			SwapBuffers ();
		}

		/// <summary>
		/// Happens before updating.
		/// </summary>
		/// <param name="e">E.</param>
		void InternalUpdate (UpdateEventArgs e) {
			if (!PreparationDone) {
				Prepare ();
				PreparationDone = true;
			}
		}
	}
}

