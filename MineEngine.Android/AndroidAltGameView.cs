﻿using System;
using System.Threading;
using Android.App;
using Android.Content;
using Android.Views;
using OpenTK.Graphics;
using OpenTK.Platform.Android;
using ES10 = OpenTK.Graphics.ES10;
using ES20 = OpenTK.Graphics.ES20;
using ES30 = OpenTK.Graphics.ES30;
using ES31 = OpenTK.Graphics.ES31;
using System.Threading.Tasks;
using OpenTK;

namespace Gimmes.MineEngine.Android {

	/// <summary>
	/// Android alternate game view.
	/// Drop-in replacement for AndroidGameView.
	/// </summary>
	public class AndroidAltGameView : SurfaceView, ISurfaceHolderCallback {
		
		/// <summary>
		/// The graphics context.
		/// </summary>
		public IGraphicsContext GraphicsContext;

		/// <summary>
		/// The context rendering API.
		/// </summary>
		public GLVersion ContextRenderingApi;

		/// <summary>
		/// The graphics mode.
		/// </summary>
		public GraphicsMode GraphicsMode;

		/// <summary>
		/// The window state.
		/// </summary>
		public AndroidWindowState WindowInfo;

		/// <summary>
		/// Return the width of the view.
		/// </summary>
		/// <value>To be added.</value>
		public new int Width {
			get { return WindowInfo.Width; }
		}

		/// <summary>
		/// Return the height of the view.
		/// </summary>
		/// <value>To be added.</value>
		public new int Height {
			get { return WindowInfo.Height; }
		}

		/// <summary>
		/// Gets the GL version.
		/// </summary>
		/// <value>The GL version.</value>
		internal int Version {
			get { return (int) ContextRenderingApi; }
		}

		/// <summary>
		/// Gets the graphics mode as <see cref="AndroidGraphicsMode"/>.
		/// Is it really better tho?
		/// </summary>
		/// <value>The better(TM) graphics mode.</value>
		internal AndroidGraphicsMode BetterGraphicsMode {
			get { return GraphicsMode as AndroidGraphicsMode; }
		}

		/// <summary>
		/// Gets the graphics context as <see cref="AndroidGraphicsContext"/>.
		/// </summary>
		/// <value>The better(TM) graphics context.</value>
		internal AndroidGraphicsContext BetterGraphicsContext {
			get { return GraphicsContext as AndroidGraphicsContext; }
		}

		/// <summary>
		/// The ready reset event.
		/// </summary>
		ManualResetEvent ReadyResetEvent;

		/// <summary>
		/// Whether the surface was created.
		/// </summary>
		bool IsSurfaceCreated;

		/// <summary>
		/// Whether the context was created.
		/// </summary>
		bool IsGraphicsContextCreated;

		/// <summary>
		/// Initializes a new instance of the <see cref="Gimmes.MineEngine.Android.AndroidAltGameView"/> class.
		/// </summary>
		/// <param name="context">Context.</param>
		public AndroidAltGameView (Context context) : base (context) {
			Initialize ();
		}

		/// <summary>
		/// Creates the frame buffer.
		/// </summary>
		protected virtual void CreateFrameBuffer () {
			AndroidLog.Write ("CALL CreateFrameBuffer");
			AndroidLog.Write ("TARGET {0} ({1})", Version, ContextRenderingApi);
			
			// Make sure that the graphics mode is valid
			if (GraphicsMode != null && !(GraphicsMode is AndroidGraphicsMode)) {
				GraphicsMode = new AndroidGraphicsMode (
					display: WindowInfo.Window.Display,
					version: Version,
					mode: GraphicsMode
				);
			}

			if (BetterGraphicsMode == null) {
				GraphicsMode = new AndroidGraphicsMode (
					display: WindowInfo.Window.Display,
					version: Version,
					mode: new GraphicsMode ()
				);
				BetterGraphicsMode.Initialize (
					display: WindowInfo.Window.Display,
					version: Version
				);
			}

			// Create the surface
			WindowInfo.CreateSurface (BetterGraphicsMode.Config);

			// We got a surface!
			IsSurfaceCreated = true;

			// Make the context current
			MakeCurrent ();
		}

		/// <summary>
		/// Makes the context current.
		/// </summary>
		protected void MakeCurrent () {
			AndroidLog.Write ("CALL MakeCurrent");

			// Create the context if needed
			if (!IsGraphicsContextCreated || GraphicsContext == null)
				CreateContext ();

			// Make the context current
			GraphicsContext.MakeCurrent (WindowInfo.Window);
		}

		/// <summary>
		/// Swaps the front and back buffers of the current GraphicsContext, presenting the rendered scene to the user.
		/// </summary>
		public void SwapBuffers () {
			AndroidLog.Write ("CALL SwapBuffers");

			// I don't even know why
			if (BetterGraphicsContext != null) {
				if (!BetterGraphicsContext.Swap ())
					CreateContext ();
			} else
				GraphicsContext.SwapBuffers ();
		}

		/// <summary>
		/// Pauses the game.
		/// </summary>
		public void Pause () {
		}

		/// <summary>
		/// Resumes the game.
		/// </summary>
		public void Resume () {
		}

		/// <summary>
		/// Initialize the game view.
		/// </summary>
		void Initialize () {
			AndroidLog.Write ("CALL Initialize");

			// Set state variables
			IsSurfaceCreated = false;
			IsGraphicsContextCreated = false;

			// Register holder callback
			Holder.AddCallback (this);

			// Create the window
			WindowInfo = AndroidWindowState.CreateFrom (Holder);

			// Set default rendering API
			ContextRenderingApi = GLVersion.ES1;

			// Create the ready reset event
			ReadyResetEvent = new ManualResetEvent (false);
		}

		/// <summary>
		/// Creates the context.
		/// </summary>
		void CreateContext () {
			AndroidLog.Write ("CALL CreateContext");

			// Create the graphics context
			try {
			GraphicsContext = new AndroidGraphicsContext (
				mode: GraphicsMode,
				window: WindowInfo.Window,
				sharedContext: GraphicsContext,
				glesVersion: ContextRenderingApi,
				flags: GraphicsContextFlags.Embedded
			);
			} catch (Exception ex) {
				AndroidLog.Write ("Error while creating context: {0}", ex.Message);
				return;
			}

			// We're done here
			IsGraphicsContextCreated = true;
		}

		/// <summary>
		/// Creates the surface.
		/// </summary>
		void CreateSurface () {
			AndroidLog.Write ("CALL CreateSurface");

			// No need to create the surface twice
			if (IsSurfaceCreated)
				return;

			// Create the frame buffer if needed
			if (BetterGraphicsMode == null || BetterGraphicsMode.Config == null) {

				// Create the frame buffer
				CreateFrameBuffer ();
			} else {
				
				// Initialize the graphics mode
				BetterGraphicsMode.Initialize (WindowInfo.Window.Display, Version);

				// Create the surface for real
				WindowInfo.CreateSurface (BetterGraphicsMode.Config);

				// By now we should be able to work with the surface
				IsSurfaceCreated = true;
			}

			// Make the context current
			MakeCurrent ();

			// Set the ready reset event
			ReadyResetEvent.Set ();
		}

		/// <summary>
		/// Enters the game loop.
		/// </summary>
		void EnterGameLoop () {
			AndroidLog.Write ("CALL EnterGameLoop");

			// Start the update/render task
			Task.Factory.StartNew (() => {
				
				// Wait till the view is ready for rendering
				AndroidLog.Write ("AWAIT ReadyResetEvent");
				ReadyResetEvent.WaitOne ();
				AndroidLog.Write ("CONTINUE EnterGameLoop");

				// Enter the update/render loop
				while (true) {

					// TODO: Calculate delta time
					var e = new FrameEventArgs (elapsed: 0);

					// Call the update/render functions in the UI thread
					Application.SynchronizationContext.Send (_ => {
						if (!IsSurfaceCreated)
							return;
						OnUpdateFrame (e);
						OnRenderFrame (e);
					}, null);
				}
			});
		}

		/// <summary>
		/// Enters the game loop of the GameWindow using the maximum update rate.
		/// </summary>
		/// <seealso cref="M:OpenTK.Platform.IGameWindow.Run(System.Double)"></seealso>
		public void Run () {
			AndroidLog.Write ("CALL Run");

			// Enter the game loop
			EnterGameLoop ();
		}

		/// <summary>
		/// Enters the game loop of the GameWindow using the specified update rate.
		/// </summary>
		/// <param name="updateRate">Update rate.</param>
		public void Run (double updateRate) {
			AndroidLog.Write ("CALL Run[updateRate]");

			// TODO: Properly implement this
			Run ();
		}

		/// <summary>
		/// Happens before rendering.
		/// </summary>
		/// <param name="e">Event arguments.</param>
		protected virtual void OnUpdateFrame (FrameEventArgs e) {
		}

		/// <summary>
		/// Happens after updating.
		/// </summary>
		/// <param name="e">Event arguments.</param>
		protected virtual void OnRenderFrame (FrameEventArgs e) {
			SwapBuffers ();
		}

		#region implemented abstract members of ISurfaceHolderCallback

		/// <param name="holder">The SurfaceHolder whose surface has changed.</param>
		/// <param name="format">The new PixelFormat of the surface.</param>
		/// <param name="width">The new width of the surface.</param>
		/// <param name="height">The new height of the surface.</param>
		/// <summary>
		/// This is called immediately after any structural changes (format or
		///  size) have been made to the surface.
		/// </summary>
		public void SurfaceChanged (ISurfaceHolder holder, global::Android.Graphics.Format format, int width, int height) {
			AndroidLog.Write ("CALL SurfaceChanged");

			// Declare the actions for glViewport and glScissor
			var dummy = new Action<int, int, int, int> ((_1, _2, _3, _4) => { });
			var glViewport = dummy;
			var glScissor = dummy;

			// Make sure to use the right version of
			// glViewport and glScissor
			switch (ContextRenderingApi) {
			case GLVersion.ES1:
				glViewport = ES10.GL.Viewport;
				glScissor = ES10.GL.Scissor;
				break;
			case GLVersion.ES2:
				glViewport = ES20.GL.Viewport;
				glScissor = ES20.GL.Scissor;
				break;
			case GLVersion.ES3:
				glViewport = ES30.GL.Viewport;
				glScissor = ES30.GL.Scissor;
				break;
			case GLVersion.ES31:
				glViewport = ES31.GL.Viewport;
				glScissor = ES31.GL.Scissor;
				break;
			}

			// Set the viewport
			glViewport (0, 0, WindowInfo.Width, WindowInfo.Height);
			glScissor (0, 0, WindowInfo.Width, WindowInfo.Height);
		}

		/// <param name="holder">The SurfaceHolder whose surface is being created.</param>
		/// <summary>
		/// This is called immediately after the surface is first created.
		/// </summary>
		public void SurfaceCreated (ISurfaceHolder holder) {
			AndroidLog.Write ("CALL SurfaceCreated");

			// Create the surface if needed
			CreateSurface ();
		}

		/// <param name="holder">The SurfaceHolder whose surface is being destroyed.</param>
		/// <summary>
		/// This is called immediately before a surface is being destroyed.
		/// </summary>
		public void SurfaceDestroyed (ISurfaceHolder holder) {
			AndroidLog.Write ("CALL SurfaceDestroyed");

			// Destroy the surface
			WindowInfo.DestroySurface ();
		}
		#endregion
	}
}

