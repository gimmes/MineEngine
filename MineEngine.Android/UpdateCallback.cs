﻿using System;
using OpenTK;

namespace Gimmes.MineEngine.Android {

	/// <summary>
	/// Mine game update/render callback.
	/// </summary>
	public delegate void UpdateCallback (UpdateEventArgs e);
}

