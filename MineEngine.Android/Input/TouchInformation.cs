﻿using System;
using Android.Views;
using OpenTK;
using Android.Util;

namespace Gimmes.MineEngine.Android {

	/// <summary>
	/// Touch information.
	/// </summary>
	public class TouchInformation {

		/// <summary>
		/// The tag.
		/// </summary>
		const string TAG = "TouchInformation";

		/// <summary>
		/// The position.
		/// </summary>
		public Vector2 Position;

		/// <summary>
		/// Gets a value indicating whether the screen is currently being touched.
		/// </summary>
		public bool IsUp { get { return !IsDown; } }

		/// <summary>
		/// Gets a value indicating whether the screen is currently being touched.
		/// </summary>
		public bool IsDown { get; private set; }

		/// <summary>
		/// Initializes a new instance of the <see cref="Gimmes.MineEngine.TouchInformation"/> class.
		/// </summary>
		public TouchInformation () {
			Position = Vector2.Zero;
			IsDown = false;
		}

		/// <summary>
		/// Initializes a new instance of the <see cref="Gimmes.MineEngine.TouchInformation"/> class.
		/// </summary>
		/// <param name="e">Event arguments.</param>
		public TouchInformation (MotionEvent e)
			: this () {
			Update (e);
		}

		/// <summary>
		/// Update the touch information.
		/// </summary>
		/// <param name="e">E.</param>
		public void Update (MotionEvent e) {
			switch (e.Action) {
			case MotionEventActions.Down:
			case MotionEventActions.Move:
				IsDown = true;
				Position.X = e.GetX ();
				Position.Y = e.GetY ();
				break;
			case MotionEventActions.Up:
				IsDown = false;
				break;
			}
		}
	}
}

