﻿using System;
using System.Collections.Generic;
using Android.App;
using Android.Content;
using Android.Views;
using OpenTK;
using OpenTK.Graphics;
using OpenTK.Platform.Android;

namespace Gimmes.MineEngine.Android {

	/// <summary>
	/// Mine game.
	/// </summary>
	public class MineGameView : AndroidAltGameView {

		/// <summary>
		/// The game configuration.
		/// </summary>
		readonly public GameConfiguration Configuration;

		/// <summary>
		/// The update callbacks.
		/// </summary>
		readonly List<UpdateCallback> UpdateCallbacks;

		/// <summary>
		/// The render callbacks.
		/// </summary>
		readonly List<UpdateCallback> RenderCallbacks;

		/// <summary>
		/// The game.
		/// </summary>
		readonly MineGame Game;

		/// <summary>
		/// Gets the touch info.
		/// </summary>
		/// <value>The touch info.</value>
		public TouchInformation TouchInformation { get; private set; }

		/// <summary>
		/// Initializes a new instance of the <see cref="Gimmes.MineEngine.Android.MineGameView"/> class.
		/// </summary>
		/// <param name="context">Context.</param>
		MineGameView (Context context) : base (context) {
			UpdateCallbacks = new List<UpdateCallback> ();
			RenderCallbacks = new List<UpdateCallback> ();
			TouchInformation = new TouchInformation ();
		}

		/// <summary>
		/// Initializes a new instance of the <see cref="Gimmes.MineEngine.Android.MineGameView"/> class.
		/// </summary>
		/// <param name="game">Game.</param>
		/// <param name="conf">Game configuration.</param>
		/// <param name="context">Context.</param>
		public MineGameView (MineGame game, GameConfiguration conf, Activity context)
			: this (context) {

			// Set game
			Game = game;

			// Set game configuration
			Configuration = conf;

			// Resize automatically
			// AutoResize = true;

			// Render on separate thread
			//AutoSetContextOnRenderFrame = false;
			//RenderOnUIThread = false;
		}

		/// <summary>
		/// Registers an update callback.
		/// </summary>
		/// <param name="callback">Callback.</param>
		public void RegisterUpdateCallback (UpdateCallback callback) {
			UpdateCallbacks.Add (callback);
		}

		/// <summary>
		/// Registers a render callback.
		/// </summary>
		/// <param name="callback">Callback.</param>
		public void RegisterRenderCallback (UpdateCallback callback) {
			RenderCallbacks.Add (callback);
		}

		/// <summary>
		/// Creates the frame buffer.
		/// </summary>
		protected override void CreateFrameBuffer () {

			// Set GL ES 2
			ContextRenderingApi = GLVersion.ES2;

			// Set the graphics mode
			/*
			var depth = (int) Configuration.ColorDepth;
			var color = new ColorFormat (bpp: depth);
			GraphicsMode = new AndroidGraphicsMode (
				color: color,
				depth: depth,
				stencil: 0,
				samples: 0,
				buffers: 2,
				stereo: Configuration.StereoscopicRendering
			);
			*/

			// Create the framebuffer
			base.CreateFrameBuffer ();
		}

		/// <summary>
		/// Handles touch screen motion events.
		/// </summary>
		/// <param name="e">The motion event.</param>
		public override bool OnTouchEvent (MotionEvent e) {
			TouchInformation.Update (e);
			return true;
		}

		/// <summary>
		/// Handles the update frame event.
		/// </summary>
		/// <param name="e">Event arguments.</param>
		protected override void OnUpdateFrame (FrameEventArgs e) {

			// Call all update callbacks
			for (var i = 0; i < UpdateCallbacks.Count; i++)
				UpdateCallbacks [i] (new UpdateEventArgs (e.Time));
		}

		/// <summary>
		/// Handles the render frame event.
		/// </summary>
		/// <param name="e">Event arguments.</param>
		protected override void OnRenderFrame (FrameEventArgs e) {

			// Call all render callbacks
			for (var i = 0; i < RenderCallbacks.Count; i++)
				RenderCallbacks [i] (new UpdateEventArgs (e.Time));
		}
	}
}

