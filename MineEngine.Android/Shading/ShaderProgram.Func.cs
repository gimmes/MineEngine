﻿using System;
using OpenTK.Graphics.ES20;
using System.Collections.Generic;

namespace Gimmes.MineEngine.Android {
	
	/// <summary>
	/// Shader program.
	/// </summary>
	public partial class ShaderProgram {

		/// <summary>
		/// Binds an attribute location.
		/// </summary>
		/// <param name="index">Index.</param>
		/// <param name="name">Name.</param>
		public void BindAttribLocation (int index, string name) {
			GL.BindAttribLocation (programId, index, name);
		}

		/// <summary>
		/// Binds an attribute location.
		/// </summary>
		/// <param name="attrs">Attrs.</param>
		public void BindAttribLocation (params KeyValuePair<int, string>[] attrs) {
			for (var i = 0; i < attrs.Length; i++)
				BindAttribLocation (attrs [i].Key, attrs [i].Value);
		}

		/// <summary>
		/// Binds an attribute location.
		/// </summary>
		/// <param name="attrs">Attrs.</param>
		public void BindAttribLocation (params Tuple<int, string>[] attrs) {
			for (var i = 0; i < attrs.Length; i++)
				BindAttribLocation (attrs [i].Item1, attrs [i].Item2);
		}
	}

}

