﻿using System;

namespace Gimmes.MineEngine.Android {

	/// <summary>
	/// A pair of vertex and fragment shaders.
	/// </summary>
	public class ShaderPair<TShaderType>
		where TShaderType : IShader {

		/// <summary>
		/// The vertex shader.
		/// </summary>
		readonly public TShaderType VertexShader;

		/// <summary>
		/// The fragment shader.
		/// </summary>
		readonly public TShaderType FragmentShader;

		/// <summary>
		/// Initializes a new instance of the <see cref="Gimmes.MineEngine.Android.ShaderPair{TShaderType}"/> class.
		/// </summary>
		/// <param name="vertexShader">Vertex shader.</param>
		/// <param name="fragmentShader">Fragment shader.</param>
		public ShaderPair (TShaderType vertexShader, TShaderType fragmentShader) {
			VertexShader = vertexShader;
			FragmentShader = fragmentShader;
		}
	}
}

