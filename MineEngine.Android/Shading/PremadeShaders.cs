﻿using System;
using OpenTK.Graphics.ES20;

namespace Gimmes.MineEngine.Android {

	/// <summary>
	/// A collection of premade shaders.
	/// </summary>
	public static class PremadeShaders {

		/// <summary>
		/// Passthrough shader.
		/// </summary>
		public static ShaderPair<ShaderSource> Passthrough;

		/// <summary>
		/// Do not use.
		/// </summary>
		public static ShaderPair<ShaderSource> Test;

		/// <summary>
		/// Initializes the <see cref="Gimmes.MineEngine.Android.PremadeShaders"/> class.
		/// </summary>
		static PremadeShaders () {

			// Test vertex shader
			var vtest = new ShaderSource (ShaderType.VertexShader, @"
			attribute vec3 v_pos;
			attribute vec2 v_tex;
			attribute vec4 v_col;
			varying vec4 f_col;
			varying vec2 f_tex;
			uniform mat4 MVP;
			void main () {
				f_col = v_col;
				f_tex = v_tex;
				gl_Position = MVP * vec4 (v_pos, 1.0);
			}");

			// Test fragment shader
			var ftest = new ShaderSource (ShaderType.FragmentShader, @"
			varying vec4 f_col;
			varying vec2 f_tex;
			varying vec4 frag_color;
			uniform sampler2D tex;
			void main () {
				gl_FragColor = f_col;
				gl_FragColor = texture2D (tex, f_tex) * f_col;
			}");

			// Test shader pair
			Test = new ShaderPair<ShaderSource> (vtest, ftest);

			// Passthrough vertex shader
			var vpassthrough = new ShaderSource (ShaderType.VertexShader, @"
			#version 100
			attribute vec3 v_pos;
			attribute vec3 v_col;
			varying vec3 f_col;
			void main () {
				f_col = v_col;
				gl_Position = vec4 (v_pos, 1.0);
			}");

			// Passthrough fragment shader
			var fpassthrough = new ShaderSource (ShaderType.FragmentShader, @"
			#version 100
			precision mediump float;
			varying vec3 f_col;
			void main () {
				gl_FragColor = vec4 (f_col, 1.0);
			}");

			// Passthrough shader pair
			Passthrough = new ShaderPair<ShaderSource> (vpassthrough, fpassthrough);
		}
	}
}

