﻿using System;
using Android.Util;
using OpenTK.Graphics.ES20;

namespace Gimmes.MineEngine.Android
{
	/// <summary>
	/// Basic shader.
	/// </summary>
	public class BasicShader : IShader, IDisposable {

		/// <summary>
		/// The type of the shader.
		/// </summary>
		readonly public ShaderType ShaderType;

		/// <summary>
		/// The sources.
		/// </summary>
		string[] shaderSources;

		/// <summary>
		/// The shader identifier.
		/// </summary>
		int internalShaderId;

		/// <summary>
		/// Gets the shader identifier.
		/// </summary>
		/// <value>The shader identifier.</value>
		public int ShaderId {
			get { return internalShaderId; }
		}
		
		/// <summary>
		/// Initializes a new instance of the <see cref="MineEngine.Android.BasicShader"/> class.
		/// Call <see cref="Compile"/> to compile the shader.
		/// </summary>
		/// <param name="type">Type.</param>
		/// <param name="sources">Sources.</param>
		public BasicShader (ShaderType type, params string[] sources) {
			ShaderType = type;
			shaderSources = sources;
		}

		/// <summary>
		/// Compile the shader.
		/// </summary>
		public void Compile () {

			// Create the shader
			internalShaderId = GL.CreateShader (ShaderType);

			var lengths = new int[shaderSources.Length];
			for (var i = 0; i < lengths.Length; i++)
				lengths [i] = -1;

			// Load the shader sources
			GL.ShaderSource (internalShaderId, shaderSources [0]);

			// Compile the shader
			GL.CompileShader (internalShaderId);

			// Get the shader compile status
			int status;
			GL.GetShader (
				shader: internalShaderId,
				pname: ShaderParameter.CompileStatus,
				@params: out status
			);

			// Check if there was an error compiling the shader
			if (status == 0) {
				
				// Throw an exception
				throw new ShaderException (ShaderType, internalShaderId);
			}
		}

		/// <summary>
		/// Releases all resource used by the <see cref="MineEngine.Android.BasicShader"/> object.
		/// </summary>
		/// <remarks>Call <see cref="Dispose"/> when you are finished using the <see cref="MineEngine.Android.BasicShader"/>. The
		/// <see cref="Dispose"/> method leaves the <see cref="MineEngine.Android.BasicShader"/> in an unusable state. After calling
		/// <see cref="Dispose"/>, you must release all references to the <see cref="MineEngine.Android.BasicShader"/> so the garbage
		/// collector can reclaim the memory that the <see cref="MineEngine.Android.BasicShader"/> was occupying.</remarks>
		public void Dispose () {

			// Delete the shader if its id is not -1
			if (internalShaderId != -1)
				GL.DeleteShader (internalShaderId);

			// Set the shader id to -1
			internalShaderId = -1;
		}
	}
}

