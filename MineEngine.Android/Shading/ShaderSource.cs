﻿using System;
using OpenTK.Graphics.ES20;

namespace Gimmes.MineEngine.Android {
	
	public class ShaderSource : IShader {

		/// <summary>
		/// The shader source code.
		/// </summary>
		readonly public string Source;

		/// <summary>
		/// The shader type.
		/// </summary>
		readonly public ShaderType ShaderType;

		/// <summary>
		/// The compilation result.
		/// </summary>
		public BasicShader CompilationResult;

		/// <summary>
		/// Initializes a new instance of the <see cref="Gimmes.MineEngine.Android.ShaderSource"/> class.
		/// </summary>
		/// <param name="type">Type.</param>
		/// <param name="source">Source.</param>
		public ShaderSource (ShaderType type, string source) {
			Source = source;
			ShaderType = type;
		}

		/// <summary>
		/// Compile the shader.
		/// The result is stored in <see cref="CompilationResult"/>. 
		/// </summary>
		public void Compile () {
			if (CompilationResult == null)
				CompilationResult = CompileNew ();
		}

		/// <summary>
		/// Compile the shader.
		/// </summary>
		public BasicShader CompileNew () {
			var shader = new BasicShader (ShaderType, Source);
			shader.Compile ();
			return shader;
		}
	}
}

