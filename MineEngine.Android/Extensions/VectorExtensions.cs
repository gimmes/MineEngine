﻿using System;
using OpenTK;

namespace Gimmes.MineEngine.Android {

	/// <summary>
	/// Vector extensions
	/// </summary>
	public static class VectorExtensions {

		/// <summary>
		/// Returns a normalized version of the given vector.
		/// </summary>
		/// <param name="vec">Vector.</param>
		public static Vector3 Normalized (this Vector3 vec) {
			var newVec = new Vector3 (vec.X, vec.Y, vec.Z);
			newVec.Normalize ();
			return newVec;
		}
	}
}

