﻿using System;
using Android.Util;

namespace Gimmes.MineEngine.Android {

	/// <summary>
	/// Android log.
	/// </summary>
	public static class AndroidLog {

		/// <summary>
		/// The tag.
		/// </summary>
		readonly static string Tag = "MineEngine";

		/// <summary>
		/// Write the specified message to the log.
		/// </summary>
		/// <param name="format">Format.</param>
		/// <param name="args">Arguments.</param>
		public static void Write (string format, params object[] args) {
			Log.Debug (Tag, format, args);
		}
	}
}

