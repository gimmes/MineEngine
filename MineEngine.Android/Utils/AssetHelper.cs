﻿using System;
using Android.Graphics;
using Android.Content.Res;
using Android.Content;

namespace Gimmes.MineEngine.Android {
	
	public class AssetHelper {

		/// <summary>
		/// The context.
		/// </summary>
		readonly Context Context;

		/// <summary>
		/// Initializes a new instance of the <see cref="Gimmes.MineEngine.Android.AssetHelper"/> class.
		/// </summary>
		/// <param name="context">Context.</param>
		public AssetHelper (Context context) {
			Context = context;
		}
	}
}

