﻿using System;
using OpenTK;

namespace Gimmes.MineEngine.Android {

	/// <summary>
	/// Matrix4 helper.
	/// </summary>
	public static class Matrix4Helper {

		/// <summary>
		/// Creates a rotation matrix from a quaternion.
		/// </summary>
		/// <param name="q">The quaternion to rotate by.</param>
		public static Matrix4 FromQuaternion (Quaternion q) {
			Vector3 axis;
			float angle;
			q.ToAxisAngle(out axis, out angle);
			Matrix4 result;
			Matrix4.CreateFromAxisAngle (axis, angle, out result);
			return result;
		}
	}
}

