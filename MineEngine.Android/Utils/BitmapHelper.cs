﻿using System;
using Android.Content;
using Android.Graphics;

namespace Gimmes.MineEngine.Android {

	/// <summary>
	/// Bitmap helper.
	/// </summary>
	public static class BitmapHelper {

		/// <summary>
		/// Determines whether subsampling is needed.
		/// </summary>
		/// <returns><c>true</c>, if subsampling is needed, <c>false</c> otherwise.</returns>
		/// <param name="info">Info.</param>
		/// <param name="targetWidth">Target width.</param>
		/// <param name="targetHeight">Target height.</param>
		/// <param name="factor">Factor.</param>
		public static bool TryGetSubsamplingFactor (
			BitmapInformation info,
			int targetWidth, int targetHeight,
			out int factor) {
			factor = GetSubsamplingFactor (
				info: info,
				targetWidth: targetWidth,
				targetHeight: targetHeight
			);
			return factor != 0;
		}

		/// <summary>
		/// Gets the optimal subsampling factor.
		/// </summary>
		/// <returns>The optimal subsampling factor.</returns>
		/// <param name="info">Info.</param>
		/// <param name="targetWidth">Target width.</param>
		/// <param name="targetHeight">Target height.</param>
		public static int GetSubsamplingFactor (
			BitmapInformation info,
			int targetWidth, int targetHeight) {
			int factor = 1;
			if (false
			    || (targetWidth > info.Width)
			    || (targetHeight > info.Height)) {
				int hw = info.Width / 2;
				int hh = info.Height / 2;
				while (true
				       && (hw / factor) > targetWidth
				       && (hh / factor) > targetHeight)
					factor *= 2;
			} else
				return 0;
			return factor;
		}

		/// <summary>
		/// Gets bitmap information without loading it into memory.
		/// </summary>
		/// <returns>The bitmap information.</returns>
		/// <param name="context">The context.</param>
		/// <param name="resid">Resid.</param>
		public static BitmapInformation GetBitmapInformation (Context context, int resid) {
			BitmapInformation info;
			using (var options = new BitmapFactory.Options ())
				info = GetBitmapInformation (context, resid, options);
			return info;
		}

		/// <summary>
		/// Gets bitmap information without loading it into memory.
		/// Keeps the options intact.
		/// </summary>
		/// <returns>The bitmap information.</returns>
		/// <param name="context">Context.</param>
		/// <param name="resid">Resid.</param>
		/// <param name="options">Options.</param>
		public static BitmapInformation GetBitmapInformation (
			Context context, int resid, BitmapFactory.Options options) {
			options.InJustDecodeBounds = true;
			BitmapFactory.DecodeResource (
				res: context.Resources,
				id: resid,
				opts: options
			);
			options.InJustDecodeBounds = false;
			return new BitmapInformation (options, dispose: false);
		}

		/// <summary>
		/// Sets the optimal subsampling factor.
		/// Does not do anything if subsampling is not needed.
		/// </summary>
		/// <param name="context">Context.</param>
		/// <param name="options">Options.</param>
		/// <param name="resid">Resid.</param>
		/// <param name="targetWidth">Target width.</param>
		/// <param name="targetHeight">Target height.</param>
		public static void SetOptimalSubsamplingFactor (
			Context context, BitmapFactory.Options options,
			int resid, int targetWidth, int targetHeight) {
			if (targetWidth > 0 && targetHeight > 0) {
				var info = GetBitmapInformation (
					context: context,
					resid: resid,
					options: options
				);
				int factor;
				if (TryGetSubsamplingFactor (
					info: info,
					targetWidth: targetWidth,
					targetHeight: targetHeight,
					factor: out factor
				)) {
					options.InSampleSize = factor;
				}
			}
		}
	}
}

