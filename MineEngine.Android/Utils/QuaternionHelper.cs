﻿using System;
using OpenTK;

namespace Gimmes.MineEngine.Android {

	/// <summary>
	/// Quaternion helper.
	/// </summary>
	public static class QuaternionHelper {

		/// <summary>
		/// Constructs a quaternion from euler angles.
		/// </summary>
		/// <returns>The quaternion.</returns>
		/// <param name="pitch">The pitch (attitude), rotation around X axis</param>
		/// <param name="yaw">The yaw (heading), rotation around Y axis</param>
		/// <param name="roll">The roll (bank), rotation around Z axis</param>
		public static Quaternion FromEulerAngles (float pitch, float yaw, float roll) {
			yaw *= 0.5f;
			roll *= 0.5f;
			pitch *= 0.5f;
			float c1 = (float) Math.Cos (yaw);
			float c2 = (float) Math.Cos (pitch);
			float c3 = (float) Math.Cos (roll);
			float s1 = (float) Math.Sin (yaw);
			float s2 = (float) Math.Sin (pitch);
			float s3 = (float) Math.Sin (roll);
			var result = Quaternion.Identity;
			result.W = c1 * c2 * c3 - s1 * s2 * s3;
			result.X = s1 * s2 * c3 + c1 * c2 * s3;
			result.Y = s1 * c2 * c3 + c1 * s2 * s3;
			result.Z = c1 * s2 * c3 - s1 * c2 * s3;
			return result;
		}
	}
}

