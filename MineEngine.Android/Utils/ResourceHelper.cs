﻿using System;
using Android.Content;
using Android.Graphics;

namespace Gimmes.MineEngine.Android {

	/// <summary>
	/// Resource helper.
	/// </summary>
	public class ResourceHelper {

		/// <summary>
		/// The context.
		/// </summary>
		readonly Context Context;

		/// <summary>
		/// Initializes a new instance of the <see cref="Gimmes.MineEngine.Android.ResourceHelper"/> class.
		/// </summary>
		/// <param name="context">Context.</param>
		public ResourceHelper (Context context) {
			Context = context;
		}

		/// <summary>
		/// Gets the resource identifier.
		/// </summary>
		/// <returns>The resource identifier.</returns>
		/// <param name="name">Name.</param>
		/// <param name="deftype">Deftype.</param>
		public int GetResourceId (string name, string deftype) {
			var package = Context.ApplicationInfo.PackageName;
			var id = Context.Resources.GetIdentifier (
				name: name,
				defType: deftype,
				defPackage: package
			);
			if (id == 0)
				throw new ArgumentException (string.Format ("Resource '{0}' not found.", name));
			return id;
		}

		/// <summary>
		/// Tries the get the resource identifier.
		/// </summary>
		/// <returns><c>true</c>, if the operation was successful, <c>false</c> otherwise.</returns>
		/// <param name="name">Name.</param>
		/// <param name="deftype">Deftype.</param>
		/// <param name="resid">Resid.</param>
		public bool TryGetResourceId (string name, string deftype, out int resid) {
			resid = 0;
			try {
				resid = GetResourceId (name, deftype);
			} catch (ArgumentException) {
				return false;
			}
			return true;
		}

		/// <summary>
		/// Loads a bitmap from the resources.
		/// </summary>
		/// <returns>The bitmap.</returns>
		/// <param name="name">Name.</param>
		/// <param name="deftype">Deftype.</param>
		public Bitmap GetBitmap (string name, string deftype = "textures") {
			return GetBitmap (name, 0, 0, deftype);
		}

		/// <summary>
		/// Loads a bitmap from the resources.
		/// </summary>
		/// <returns>The bitmap.</returns>
		/// <param name="name">Name.</param>
		/// <param name="targetWidth">Target width.</param>
		/// <param name="targetHeight">Target height.</param>
		/// <param name="deftype">Deftype.</param>
		public Bitmap GetBitmap (string name,
			int targetWidth, int targetHeight, string deftype = "textures") {
			Bitmap bitmap;
			var resid = GetResourceId (name, deftype);
			using (var options = new BitmapFactory.Options ()) {
				BitmapHelper.SetOptimalSubsamplingFactor (
					context: Context,
					options: options,
					resid: resid,
					targetWidth: targetWidth,
					targetHeight: targetHeight
				);
				bitmap = BitmapFactory.DecodeResource (
					res: Context.Resources,
					id: resid,
					opts: options
				);
			}
			return bitmap;
		}
	}
}

