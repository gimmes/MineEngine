﻿using System;
using Android.Graphics;

namespace Gimmes.MineEngine.Android {

	/// <summary>
	/// Bitmap information.
	/// </summary>
	public class BitmapInformation {

		/// <summary>
		/// The width.
		/// </summary>
		public int Width;

		/// <summary>
		/// The height.
		/// </summary>
		public int Height;

		/// <summary>
		/// The MIME type.
		/// </summary>
		public string MimeType;

		/// <summary>
		/// Initializes a new instance of the <see cref="Gimmes.MineEngine.BitmapInformation"/> class.
		/// </summary>
		/// <param name="options">Options.</param>
		/// <param name = "dispose">Whether the <paramref name="options"/> should be disposed.</param>
		public BitmapInformation (BitmapFactory.Options options, bool dispose = true) {
			Width = options.OutWidth;
			Height = options.OutHeight;
			MimeType = options.OutMimeType;
			if (dispose)
				options.Dispose ();
		}
	}
}

