﻿using System;
using Android.Opengl;
using OpenTK.Graphics.ES20;
using AndroidGraphics = Android.Graphics;

namespace Gimmes.MineEngine.Android {

	/// <summary>
	/// 2D Texture.
	/// </summary>
	public class Texture2D {

		/// <summary>
		/// Dot texture.
		/// </summary>
		readonly public static Texture2D Dot;

		static Texture2D () {
		}

		/// <summary>
		/// The texture identifier.
		/// </summary>
		readonly public int TextureId;

		/// <summary>
		/// Gets the width.
		/// </summary>
		/// <value>The width.</value>
		public int Width { get; private set; }

		/// <summary>
		/// Gets the height.
		/// </summary>
		/// <value>The height.</value>
		public int Height { get; private set; }

		/// <summary>
		/// Initializes a new instance of the <see cref="Gimmes.MineEngine.Android.Texture2D"/> class.
		/// </summary>
		/// <param name="bitmap">Bitmap.</param>
		/// <param name="preserveBitmap">Whether to preserve the bitmap.</param> 
		public Texture2D (AndroidGraphics.Bitmap bitmap, bool preserveBitmap = false) {

			// Set width and height
			Width = bitmap.Width;
			Height = bitmap.Height;

			// Lock bitmap data
			// var data = bitmap.LockPixels ();

			// Generate the texture id
			TextureId = GL.GenTexture ();

			// Bind the texture
			Bind (TextureUnit.Texture0);

			// Set the min and mag filters
			var minFilter = TextureMinFilter.Nearest;
			var magFilter = TextureMagFilter.Linear;

			// Set the min filter parameter
			GL.TexParameter (
				target: TextureTarget.Texture2D,
				pname: TextureParameterName.TextureMinFilter,
				param: (int) minFilter
			);

			// Set the mag filter parameter
			GL.TexParameter (
				target: TextureTarget.Texture2D,
				pname: TextureParameterName.TextureMagFilter,
				param: (int) magFilter
			);

			// Create the texture
			/*
			GL.TexImage2D (
				target: TextureTarget.Texture2D,
				level: 0,
				internalformat: PixelInternalFormat.Rgba,
				width: Width,
				height: Height,
				border: 0,
				format: PixelFormat.Rgba,
				type: PixelType.UnsignedByte,
				pixels: data
			);
			*/

			GLUtils.TexImage2D ((int) All.Texture2D, 0, bitmap, 0);

			// Unlock bitmap data
			//bitmap.UnlockPixels ();

			// Recycle the bitmap
			//if (!preserveBitmap)
			//	bitmap.Recycle ();

			// Unbind the texture
			Unbind (TextureUnit.Texture0);
		}

		/// <summary>
		/// Update the specified bitmap and preserveBitmap.
		/// </summary>
		/// <param name="bitmap">Bitmap.</param>
		/// <param name="preserveBitmap">Whether to suppress the bitmap.</param>
		public void Update (AndroidGraphics.Bitmap bitmap, bool preserveBitmap) {

			// Lock bitmap data
			var data = bitmap.LockPixels ();

			// Bind the texture
			Bind (TextureUnit.Texture0);

			// Set the texture data
			GL.TexSubImage2D (
				target: TextureTarget.Texture2D,
				level: 0,
				xoffset: 0,
				yoffset: 0,
				width: Width,
				height: Height,
				format: PixelFormat.Rgba,
				type: PixelType.UnsignedByte,
				pixels: data
			);

			// Unlock bitmap data
			bitmap.UnlockPixels ();

			// Recycle the bitmap
			if (!preserveBitmap)
				bitmap.Recycle ();

			// Unbind the texture
			Unbind (TextureUnit.Texture0);

		}

		/// <summary>
		/// Bind the texture.
		/// </summary>
		public void Bind () {

			// Bind the texture to texture unit 0
			Bind (TextureUnit.Texture0);
		}

		/// <summary>
		/// Bind the texture.
		/// </summary>
		public void Bind (TextureUnit unit) {

			// Make the texture unit the active one
			GL.ActiveTexture (unit);

			// Bind the texture
			GL.BindTexture (TextureTarget.Texture2D, TextureId);
		}

		/// <summary>
		/// Unbind the texture.
		/// </summary>
		public void Unbind () {

			// Unbind the texture value from texture unit 0
			Unbind (TextureUnit.Texture0);
		}

		/// <summary>
		/// Unbind the texture.
		/// </summary>
		public void Unbind (TextureUnit unit) {

			// Make the texture unit the active one
			GL.ActiveTexture (unit);

			// Unbind the texture
			GL.BindTexture (TextureTarget.Texture2D, 0);
		}

		#region Operators
		public static implicit operator int (Texture2D tex) {
			return tex.TextureId;
		}
		#endregion
	}
}

