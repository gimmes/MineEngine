﻿using System;
using OpenTK.Graphics.ES20;
using System.Runtime.InteropServices;
using System.Collections.Generic;
using System.Linq;

namespace Gimmes.MineEngine.Android {

	/// <summary>
	/// Dynamic OpenGL ES buffer.
	/// </summary>
	public class GLESBufferDynamic<T> : IBuffer<int>
		where T : struct {

		/// <summary>
		/// The buffer identifier.
		/// </summary>
		readonly public int BufferId;

		/// <summary>
		/// The settings.
		/// </summary>
		public GLESBufferSettings Settings;

		public int BufferSize { get; set; }

		/// <summary>
		/// Gets the size of the element.
		/// </summary>
		/// <value>The size of the element.</value>
		public int ElementSize { get; private set; }

		/// <summary>
		/// Initializes a new instance of the <see cref="Gimmes.MineEngine.Android.GLESBufferDynamic{T}"/> class.
		/// </summary>
		/// <param name="settings">Settings.</param>
		/// <param name="elementSize">Element size.</param>
		/// <param name="startCapacity">Start capacity.</param>
		public GLESBufferDynamic (GLESBufferSettings settings, int elementSize, int startCapacity = 8192) {

			// Set the settings
			Settings = settings;

			// Set the buffer size
			BufferSize = startCapacity;

			// Set the element size
			ElementSize = elementSize;

			// Generate the buffer
			var buffers = new int[1];
			GL.GenBuffers (1, buffers);
			BufferId = buffers [0];

			// Bind the buffer
			Bind ();

			// Set the buffer data
			var sizePtr = new IntPtr (BufferSize);
			GL.BufferData (
				target: Settings.Target,
				size: sizePtr,
				data: IntPtr.Zero,
				usage: Settings.Hint
			);

			// Unbind the buffer
			Unbind ();
		}

		/// <summary>
		/// Upload data.
		/// </summary>
		/// <param name="dataArray">Data array.</param>
		public void UploadData (T[] dataArray) {

			// Check if data is null
			if (dataArray == null)
				throw new ArgumentNullException ("dataArray");

			// Calculate the buffer size
			var bufferSize = Marshal.SizeOf (dataArray[0]) * dataArray.Length;

			// Bind the buffer
			Bind ();

			// Set the buffer data
			var sizePtr = new IntPtr (bufferSize);
			GL.BufferData<T> (
				target: Settings.Target,
				size: sizePtr,
				data: dataArray,
				usage: Settings.Hint
			);

			// Unbind the buffer
			Unbind ();
		}

		/// <summary>
		/// Upload data.
		/// </summary>
		/// <param name="dataList">Data list.</param>
		public void UploadData (IList<T> dataList) {

			// Check if data is null
			if (dataList == null)
				throw new ArgumentNullException ("dataList");

			// Calculate the buffer size
			var bufferSize = Marshal.SizeOf (dataList[0]) * dataList.Count;

			// Bind the buffer
			Bind ();

			// Set the buffer data
			var sizePtr = new IntPtr (bufferSize);
			GL.BufferData<T> (
				target: Settings.Target,
				size: sizePtr,
				data: dataList.ToArray (),
				usage: Settings.Hint
			);

			// Unbind the buffer
			Unbind ();
		}

		/// <summary>
		/// Bind the specified buffer.
		/// </summary>
		/// <param name="buffer">Buffer.</param>
		/// <typeparam name="BuffType">The 1st type parameter.</typeparam>
		public static void Bind<BuffType> (GLESBufferDynamic<BuffType> buffer)
			where BuffType : struct {

			// Check if buffer is null
			if (buffer == null)
				throw new ArgumentNullException ("buffer");

			// Bind the buffer
			GL.BindBuffer (
				target: buffer.Settings.Target,
				buffer: buffer.BufferId
			);
		}

		/// <summary>
		/// Unbind the specified buffer.
		/// </summary>
		/// <param name="buffer">Buffer.</param>
		/// <typeparam name="BuffType">The 1st type parameter.</typeparam>
		public static void Unbind<BuffType>(GLESBufferDynamic<BuffType> buffer)
			where BuffType : struct {

			// Check if buffer is null
			if (buffer == null)
				throw new ArgumentNullException ("buffer");

			// Bind the buffer
			GL.BindBuffer (
				target: buffer.Settings.Target,
				buffer: 0
			);
		}

		/// <summary>
		/// Bind the buffer.
		/// </summary>
		public void Bind () {
			GLESBufferDynamic<T>.Bind (this);
		}

		/// <summary>
		/// Unbind the buffer.
		/// </summary>
		public void Unbind () {
			GLESBufferDynamic<T>.Unbind (this);
		}

		/// <summary>
		/// Point the buffer to an index.
		/// </summary>
		/// <param name="where">Where.</param>
		public void PointTo (int where) {

			// Bind buffer
			Bind ();

			// Enable vertex attribute array
			GL.EnableVertexAttribArray (where);

			// Set vertex attribute pointer
			GL.VertexAttribPointer (where, Settings.AttribSize, Settings.Type, Settings.Normalized, ElementSize, Settings.Offset);

			// Unbind buffer
			Unbind ();
		}

		/// <summary>
		/// Point the buffer to an index.
		/// </summary>
		/// <param name="where">Where.</param>
		/// <param name="offset">Offset.</param>
		public void PointTo (int where, int offset) {

			// Bind buffer
			Bind ();

			// Enable vertex attribute array
			GL.EnableVertexAttribArray (where);

			// Set vertex attribute pointer
			GL.VertexAttribPointer (where, Settings.AttribSize, Settings.Type, Settings.Normalized, ElementSize, offset);

			// Unbind buffer
			Unbind ();
		}

		/// <summary>
		/// Point the buffer to an index.
		/// </summary>
		/// <param name="where">Where.</param>
		/// <param name="other">Other.</param>
		public void PointTo (int where, params int[] other) {

			// Check if the other parameter is null
			if (other == null)
				throw new ArgumentNullException ("other");

			// Check if other contains at least two elements
			if (other.Length < 2)
				throw new ArgumentException ("Cannot set attribute pointer: other contains less than two elements");

			// Bind buffer
			Bind ();

			// Enable vertex attribute array
			GL.EnableVertexAttribArray (where);

			// Set vertex attribute pointer
			GL.VertexAttribPointer (where, other[0], Settings.Type, Settings.Normalized, ElementSize, other[1]);

			// Unbind buffer
			Unbind ();
		}
	}
}

