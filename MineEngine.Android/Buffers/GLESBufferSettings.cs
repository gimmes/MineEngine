﻿using System;
using OpenTK.Graphics.ES20;

namespace Gimmes.MineEngine.Android {
	
	/// <summary>
	/// OpenGL ES buffer settings.
	/// </summary>
	public struct GLESBufferSettings {

		/// <summary>
		/// The buffer target.
		/// </summary>
		public BufferTarget Target;

		/// <summary>
		/// The buffer usage hint.
		/// </summary>
		public BufferUsage Hint;

		/// <summary>
		/// The size of the attributes.
		/// </summary>
		public int AttribSize;

		/// <summary>
		/// The vertex attribute pointer type.
		/// </summary>
		public VertexAttribPointerType Type;

		/// <summary>
		/// Whether the buffer is normalized.
		/// </summary>
		public bool Normalized;

		/// <summary>
		/// The buffer offset.
		/// </summary>
		public int Offset;

		/// <summary>
		/// Standard configuration for statically drawn four-float objects.
		/// </summary>
		public static GLESBufferSettings StaticDraw4FloatArray = new GLESBufferSettings {
			Target = BufferTarget.ArrayBuffer,
			Hint = BufferUsage.StaticDraw,
			AttribSize = 4,
			Type = VertexAttribPointerType.Float
		};

		/// <summary>
		/// Standard configuration for streamed four-float objects.
		/// </summary>
		public static GLESBufferSettings StreamDraw4FloatArray = new GLESBufferSettings {
			Target = BufferTarget.ArrayBuffer,
			Hint = BufferUsage.StreamDraw,
			AttribSize = 4,
			Type = VertexAttribPointerType.Float
		};

		/// <summary>
		/// Standard configuration for statically drawn three-float objects.
		/// </summary>
		public static GLESBufferSettings StaticDraw3FloatArray = new GLESBufferSettings {
			Target = BufferTarget.ArrayBuffer,
			Hint = BufferUsage.StaticDraw,
			AttribSize = 3,
			Type = VertexAttribPointerType.Float
		};

		/// <summary>
		/// Standard configuration for streamed three-float objects.
		/// </summary>
		public static GLESBufferSettings StreamDraw3FloatArray = new GLESBufferSettings {
			Target = BufferTarget.ArrayBuffer,
			Hint = BufferUsage.StreamDraw,
			AttribSize = 3,
			Type = VertexAttribPointerType.Float
		};

		/// <summary>
		/// Standard configuration for statically drawn two-float objects.
		/// </summary>
		public static GLESBufferSettings StaticDraw2FloatArray = new GLESBufferSettings {
			Target = BufferTarget.ArrayBuffer,
			Hint = BufferUsage.StaticDraw,
			AttribSize = 2,
			Type = VertexAttribPointerType.Float
		};

		/// <summary>
		/// Standard configuration for streamed two-float objects.
		/// </summary>
		public static GLESBufferSettings StreamDraw2FloatArray = new GLESBufferSettings {
			Target = BufferTarget.ArrayBuffer,
			Hint = BufferUsage.StreamDraw,
			AttribSize = 2,
			Type = VertexAttribPointerType.Float
		};

		/// <summary>
		/// Standard configuration for static indices.
		/// </summary>
		public static GLESBufferSettings StaticIndices = new GLESBufferSettings {
			Target = BufferTarget.ElementArrayBuffer,
			Hint = BufferUsage.StaticDraw
		};

		/// <summary>
		/// Standard configuration for streamed indices.
		/// </summary>
		public static GLESBufferSettings StreamIndices = new GLESBufferSettings {
			Target = BufferTarget.ElementArrayBuffer,
			Hint = BufferUsage.StreamDraw
		};

		/// <summary>
		/// Standard configuration for dynamic indices.
		/// </summary>
		public static GLESBufferSettings DynamicIndices = new GLESBufferSettings {
			Target = BufferTarget.ElementArrayBuffer,
			Hint = BufferUsage.DynamicDraw
		};
	}
}

