﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using OpenTK.Graphics.ES20;

namespace Gimmes.MineEngine.Android {
	
	/// <summary>
	/// OpenGL ES buffer.
	/// </summary>
	public class GLESBuffer<T> : IBuffer<int>
		where T : struct {

		/// <summary>
		/// The buffer identifier.
		/// </summary>
		readonly public int BufferId;

		/// <summary>
		/// The settings.
		/// </summary>
		public GLESBufferSettings Settings;

		/// <summary>
		/// Gets or sets the size of the buffer.
		/// </summary>
		/// <value>The size of the buffer.</value>
		public int BufferSize { get; set; }

		/// <summary>
		/// Gets or sets the buffer.
		/// </summary>
		/// <value>The buffer.</value>
		public IList<T> Buffer { get; set; }

		/// <summary>
		/// Gets the size of the element.
		/// </summary>
		/// <value>The size of the element.</value>
		public int ElementSize {
			get { return BufferSize / Buffer.Count; }
		}

		/// <summary>
		/// Initializes a new instance of the <see cref="Gimmes.MineEngine.Android.GLESBuffer{T}"/> class.
		/// </summary>
		/// <param name="settings">The buffer settings.</param>
		/// <param name="buffer">The buffer initialization array.</param>
		public GLESBuffer (GLESBufferSettings settings, IList<T> buffer) {

			// Set the settings
			Settings = settings;

			// Set the buffer
			Buffer = buffer;

			// Calculate the buffer size
			BufferSize = Marshal.SizeOf (buffer [0]) * Buffer.Count;

			// Generate the buffer
			var buffers = new int[1];
			GL.GenBuffers (1, buffers);
			BufferId = buffers [0];

			// Bind the buffer
			Bind ();

			// Set the buffer data
			var size = new IntPtr (BufferSize);
			GL.BufferData<T> (
				target: Settings.Target,
				size: size,
				data: Buffer.ToArray (),
				usage: Settings.Hint
			);

			// Unbind the data
			Unbind ();
		}

		/// <summary>
		/// Point the buffer to an index
		/// </summary>
		/// <param name="index">Index.</param>
		public void PointTo (int index) {

			// Return if the index is invalid
			if (index == -1)
				return;

			// Bind buffer
			Bind ();

			// Enable vertex attribute array
			GL.EnableVertexAttribArray (index);

			// Set vertex attribute pointer
			var offsetPtr = new IntPtr (Settings.Offset);
			GL.VertexAttribPointer (
				indx: index,
				size: Settings.AttribSize,
				type: Settings.Type,
				normalized: Settings.Normalized,
				stride: ElementSize,
				ptr: offsetPtr
			);
		}

		public void PointTo (int where, int offset) {

			// Bind buffer
			Bind ();

			// Enable vertex attribute array
			GL.EnableVertexAttribArray (where);

			// Set vertex attribute pointer
			var offsetPtr = new IntPtr (offset);
			GL.VertexAttribPointer (
				indx: where,
				size: Settings.AttribSize,
				type: Settings.Type,
				normalized: Settings.Normalized,
				stride: ElementSize,
				ptr: offsetPtr
			);
		}

		public void PointTo (int where, params int[] other) {

			// Check if the other parameter is null
			if (other == null)
				throw new NullReferenceException ("Cannot set attribute pointer: other is null");

			// Check if other contains at least two elements
			if (other.Length < 2)
				throw new ArgumentException ("Cannot set attribute pointer: other contains less than two elements");

			// Bind buffer
			Bind ();

			// Enable vertex attribute array
			GL.EnableVertexAttribArray (where);

			// Set vertex attribute pointer
			var offsetPtr = new IntPtr (other [1]);
			GL.VertexAttribPointer (
				indx: where,
				size: other [0],
				type: Settings.Type,
				normalized: Settings.Normalized,
				stride: ElementSize,
				ptr: offsetPtr
			);

			// Unbind buffer
			Unbind ();
		}

		/// <summary>
		/// Bind the specified buffer.
		/// </summary>
		/// <param name="buffer">Buffer.</param>
		/// <typeparam name="BuffType">The 1st type parameter.</typeparam>
		public static void Bind<BuffType> (GLESBuffer<BuffType> buffer)
			where BuffType : struct {

			// Check if the buffer is null
			if (buffer == null)
				throw new NullReferenceException ("Cannot bind buffer: buffer is null");

			// Bind the buffer
			GL.BindBuffer (
				target: buffer.Settings.Target,
				buffer: buffer.BufferId
			);
		}

		/// <summary>
		/// Bind the buffer.
		/// </summary>
		public void Bind () {

			// Bind the buffer
			GLESBuffer<T>.Bind (this);
		}

		/// <summary>
		/// Unbind the specified buffer.
		/// </summary>
		/// <param name="buffer">Buffer.</param>
		/// <typeparam name="BuffType">The 1st type parameter.</typeparam>
		public static void Unbind<BuffType>(GLESBuffer<BuffType> buffer)
			where BuffType : struct {

			// Check if the buffer is null
			if (buffer == null)
				throw new NullReferenceException ("Cannot unbind buffer: buffer is null");

			// Unbind the buffer
			GL.BindBuffer (
				target: buffer.Settings.Target,
				buffer: 0
			);
		}

		/// <summary>
		/// Unbind the buffer.
		/// </summary>
		public void Unbind () {

			// Unbind the buffer
			GLESBuffer<T>.Unbind (this);
		}
	}
}

