﻿using System;

namespace Gimmes.MineEngine.Android {

	/// <summary>
	/// Sprite effects.
	/// </summary>
	public enum SpriteEffects {
		None			= 0x0,
		FlipHorizontal	= 1 << 0,
		FlipVertical	= 1 << 1,
	}
}

