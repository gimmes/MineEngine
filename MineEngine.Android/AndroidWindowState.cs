﻿using System;
using Android.Views;
using Javax.Microedition.Khronos.Egl;
using OpenTK.Platform.Android;

namespace Gimmes.MineEngine.Android {

	/// <summary>
	/// Android window state.
	/// </summary>
	public class AndroidWindowState {

		/// <summary>
		/// The holder.
		/// </summary>
		public ISurfaceHolder Holder;

		/// <summary>
		/// The window.
		/// </summary>
		public AndroidWindow Window;

		/// <summary>
		/// Gets the width.
		/// </summary>
		/// <value>The width.</value>
		public int Width {
			get { return Holder.SurfaceFrame.Width (); }
		}

		/// <summary>
		/// Gets the height.
		/// </summary>
		/// <value>The height.</value>
		public int Height {
			get { return Holder.SurfaceFrame.Height (); }
		}

		/// <summary>
		/// Creates a <see cref="AndroidWindowState"/>
		/// from a surface holder and a callback.
		/// </summary>
		/// <returns>The newly created <see cref="AndroidWindowState"/>.</returns>
		/// <param name="holder">Holder.</param>
		public static AndroidWindowState CreateFrom (ISurfaceHolder holder) {
			AndroidLog.Write ("CREATE AndroidWindowState");
			AndroidLog.Write ("Handle Holder::{0}", holder.Handle);
			var state = new AndroidWindowState ();
			state.Holder = holder;
			state.CreateWindow ();
			return state;
		}

		/// <summary>
		/// Creates the window.
		/// </summary>
		public void CreateWindow () {
			AndroidLog.Write ("CALL CreateWindow");
			Window = new AndroidWindow (Holder);
			Window.InitializeDisplay ();
			AndroidLog.Write ("HANDLE Window::{0}", Window.Display.Handle);
		}

		/// <summary>
		/// Creates the surface.
		/// </summary>
		public void CreateSurface (EGLConfig config) {
			AndroidLog.Write ("CALL AndroidWindowState::CreateSurface");
			Window.CreateSurface (config);
		}

		/// <summary>
		/// Destroys the surface.
		/// </summary>
		public void DestroySurface () {
			AndroidLog.Write ("CALL AndroidWindowState::DestroySurface");
			Window.DestroySurface ();
		}
	}
}

