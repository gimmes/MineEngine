﻿using System;
using Android.App;
using Android.Content;
using Android.Content.PM;
using Android.OS;
using Android.Runtime;
using Android.Util;
using Android.Views;
using Android.Widget;
using Gimmes.MineEngine;
using Gimmes.MineEngine.Android;

namespace MineTestGame {
	
	[Activity (
		Label = "MineTestGame",
		ConfigurationChanges = 0x0
		| ConfigChanges.Keyboard
		| ConfigChanges.KeyboardHidden
		| ConfigChanges.Navigation
		| ConfigChanges.Orientation,
		ScreenOrientation = ScreenOrientation.FullSensor,
		MainLauncher = true,
		Icon = "@mipmap/icon"
	)]
	public class MainActivity : Activity {
		MainGame Game;

		protected override void OnCreate (Bundle savedInstanceState) {
			base.OnCreate (savedInstanceState);

			// Create game view
			Game = new MainGame (this);
			Runner.Run (Game);
		}

		protected override void OnPause () {
			base.OnPause ();
			Game.Pause ();
		}

		protected override void OnResume () {
			base.OnResume ();
			Game.Resume ();
		}
	}
}


