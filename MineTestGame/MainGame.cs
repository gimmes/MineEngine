﻿using System;
using Android.App;
using Android.Util;
using Gimmes.MineEngine;
using Gimmes.MineEngine.Android;
using OpenTK;
using OpenTK.Graphics;
using OpenTK.Graphics.ES20;

namespace MineTestGame
{
	public class MainGame : MineGame {

		Random rng;
		float r, g, b;

		Texture2D textest;
		float[] cubetexdata = {
			// Triangle 1
			0.0f, 0.0f,
			0.0f, 1.0f,
			1.0f, 0.0f,
			// Triangle 2
			0.0f, 0.0f,
			1.0f, 1.0f,
			1.0f, 0.0f,
		};

		public MainGame (Activity context)
			: base (GameConfiguration.Default, context) {
			r = 0f;
			g = 0f;
			b = 0f;
			rng = new Random ();
		}

		public override void Prepare () {
			AndroidLog.Write ("Version: {0}", GL.GetString (StringName.Version));
			var bmp = Resources.GetBitmap ("exampletexture", "drawable");
			textest = new Texture2D (bmp);
			base.Prepare ();
		}

		public override void OnUpdate (UpdateEventArgs e) {
			r = TouchInformation.Position.Y / Height;
			g = TouchInformation.Position.X / Width;
			b = (Height - TouchInformation.Position.Y) / Height;
			base.OnUpdate (e);
		}

		public override void OnRender (UpdateEventArgs e) {
			GL.ClearColor (r, g, b, 1f);
			GL.Clear (ClearBufferMask.ColorBufferBit | ClearBufferMask.DepthBufferBit);
			// AndroidLog.Write ("Texture id: {0}", textest.TextureId);
			var x = (Width / 2f) - (textest.Width / 2f);
			var y = (Height / 2f) - (textest.Height / 2f);
			var pos = new Vector2 (x, y);
			SpriteBatch.Begin ();
			SpriteBatch.Draw (textest, pos, Color4.White);
			SpriteBatch.End ();
			base.OnRender (e);
		}
	}
}

