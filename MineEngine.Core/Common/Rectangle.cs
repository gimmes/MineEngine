﻿using System;

namespace Gimmes.MineEngine {

	/// <summary>
	/// Rectangle.
	/// </summary>
	public struct Rectangle {

		/// <summary>
		/// The X coordinate.
		/// </summary>
		public int X;

		/// <summary>
		/// The Y coordinate.
		/// </summary>
		public int Y;

		/// <summary>
		/// The width.
		/// </summary>
		public int Width;

		/// <summary>
		/// The height.
		/// </summary>
		public int Height;

		/// <summary>
		/// Initializes a new instance of the <see cref="Gimmes.MineEngine.Rectangle"/> struct.
		/// </summary>
		/// <param name="x">The x coordinate.</param>
		/// <param name="y">The y coordinate.</param>
		/// <param name="width">The width.</param>
		/// <param name="height">The height.</param>
		public Rectangle (int x, int y, int width, int height)
			: this () {
			X = x;
			Y = y;
			Width = width;
			Height = height;
		}
	}
}

