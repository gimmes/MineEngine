﻿using System;

namespace Gimmes.MineEngine {
	
	/// <summary>
	/// Game configuration.
	/// </summary>
	public class GameConfiguration {

		/// <summary>
		/// Default <see cref="GameConfiguration"/>. 
		/// </summary>
		public static GameConfiguration Default;

		/// <summary>
		/// Initializes the static parts of the <see cref="MineEngine.GameConfiguration"/> class.
		/// </summary>
		static GameConfiguration () {

			// Create default configuration
			Default = new GameConfiguration {
				TargetFPS = 60,
				ColorDepth = ColorDepth.Bpp16,
				StereoscopicRendering = false,
			};
		}

		/// <summary>
		/// Gets or sets the target frames per second.
		/// </summary>
		/// <value>The target fps.</value>
		public int TargetFPS { get; set; }

		/// <summary>
		/// Gets or sets the color depth.
		/// </summary>
		/// <value>The color depth.</value>
		public ColorDepth ColorDepth { get; set; }

		/// <summary>
		/// Gets or sets a value indicating whether the game should use stereoscopic rendering.
		/// </summary>
		public bool StereoscopicRendering { get; set; }

		/// <summary>
		/// Initializes a new instance of the <see cref="Gimmes.MineEngine.GameConfiguration"/> class.
		/// </summary>
		public GameConfiguration () {
			TargetFPS = 60;
			ColorDepth = ColorDepth.Bpp16;
			StereoscopicRendering = false;
		}

		/// <summary>
		/// Initializes a new instance of the <see cref="MineEngine.GameConfiguration"/> class.
		/// </summary>
		/// <param name="fps">Target frames per second.</param>
		/// <param name="depth">The color depth.</param> 
		/// <param name="stereo">Whether to use stereoscopic rendering.</param>
		public GameConfiguration (int fps = 60, ColorDepth depth = ColorDepth.Bpp16, bool stereo = false)
			: this () {
			TargetFPS = fps;
			ColorDepth = depth;
			StereoscopicRendering = stereo;
		}
	}
}

