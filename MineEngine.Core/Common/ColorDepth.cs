﻿using System;

namespace Gimmes.MineEngine {

	/// <summary>
	/// Color depth.
	/// </summary>
	public enum ColorDepth {

		/// <summary>
		/// Default value
		/// </summary>
		Bpp0	= 0,

		/// <summary>
		/// 8-bit color depth
		/// </summary>
		Bpp8	= 8,

		/// <summary>
		/// Recommended: 16-bit color depth
		/// </summary>
		Bpp16	= 16,

		/// <summary>
		/// Recommended: 32-bit color depth
		/// </summary>
		Bpp32	= 32,
	};
}

