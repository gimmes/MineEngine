﻿using System;

namespace Gimmes.MineEngine.Android {

	/// <summary>
	/// Type switch.
	/// </summary>
	public static class TypeSwitch {

		/// <summary>
		/// Switch on TSource type
		/// </summary>
		/// <param name="value">Value.</param>
		/// <typeparam name="TSource">The 1st type parameter.</typeparam>
		public static Switch<TSource> On<TSource> (TSource value) {
			return new Switch<TSource> (value);
		}

		/// <summary>
		/// Switch.
		/// </summary>
		public sealed class Switch<TSource> {

			/// <summary>
			/// The value.
			/// </summary>
			readonly TSource value;

			/// <summary>
			/// Whether the value was handled.
			/// </summary>
			bool handled;

			/// <summary>
			/// Initializes a new instance of the <see cref="Gimmes.MineEngine.TypeSwitch"/> class.
			/// </summary>
			/// <param name="value">Value.</param>
			internal Switch (TSource value) {
				this.value = value;
			}

			/// <summary>
			/// Case.
			/// </summary>
			/// <param name="action">Action.</param>
			/// <typeparam name="TTarget">The 1st type parameter.</typeparam>
			public Switch<TSource> Case<TTarget> (Action<TTarget> action)
				where TTarget : TSource {
				if (!handled && value is TTarget) {
					action((TTarget) value);
					handled = true;
				}
				return this;
			}

			/// <summary>
			/// Default action.
			/// </summary>
			/// <param name="action">Action.</param>
			public void Default (Action<TSource> action) {
				if (!handled)
					action (value);
			}
		}
	}
}

