﻿using System;

namespace Gimmes.MineEngine {

	/// <summary>
	/// Resolution.
	/// </summary>
	public class Resolution {

		/// <summary>
		/// The width.
		/// </summary>
		readonly public int Width;

		/// <summary>
		/// The height.
		/// </summary>
		readonly public int Height;

		/// <summary>
		/// Gets the aspect ratio.
		/// </summary>
		/// <value>The aspect ratio.</value>
		public float AspectRatio {
			get { return (float) Width / (float) Height; }
		}

		/// <summary>
		/// Initializes a new instance of the <see cref="Gimmes.MineEngine.Resolution"/> class.
		/// </summary>
		/// <param name="width">Width.</param>
		/// <param name="height">Height.</param>
		public Resolution (int width, int height) {
			Width = width;
			Height = height;
		}
	}
}

