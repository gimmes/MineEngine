﻿using System;

namespace Gimmes.MineEngine {

	/// <summary>
	/// Update event arguments.
	/// </summary>
	public class UpdateEventArgs {

		/// <summary>
		/// The delta time with single precision.
		/// </summary>
		public float DeltaTime;

		/// <summary>
		/// The delta time with double precision.
		/// </summary>
		public double DeltaTime2;

		/// <summary>
		/// Initializes a new instance of the <see cref="MineEngine.UpdateEventArgs"/> class.
		/// </summary>
		/// <param name="deltaTime">Delta time.</param>
		public UpdateEventArgs (float deltaTime) {
			DeltaTime = deltaTime;
			DeltaTime2 = deltaTime;
		}

		public UpdateEventArgs (double deltaTime) {
			DeltaTime = (float) deltaTime;
			DeltaTime2 = deltaTime;
		}
	}
}

