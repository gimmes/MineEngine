﻿using System;

namespace Gimmes.MineEngine {

	/// <summary>
	/// Mine game base.
	/// </summary>
	public abstract class MineGameBase : IDisposable {

		/// <summary>
		/// Gets the width.
		/// </summary>
		/// <value>The width.</value>
		public abstract int Width { get; }

		/// <summary>
		/// Gets the height.
		/// </summary>
		/// <value>The height.</value>
		public abstract int Height { get; }

		/// <summary>
		/// Gets the configuration.
		/// </summary>
		/// <value>The configuration.</value>
		public virtual GameConfiguration Configuration { get; private set; }

		/// <summary>
		/// Gets the aspect ratio.
		/// </summary>
		/// <value>The aspect ratio.</value>
		public float AspectRatio {
			get { return (float) Width / (float) Height; }
		}

		/// <summary>
		/// Initializes a new instance of the <see cref="Gimmes.MineEngine.MineGameBase"/> class.
		/// </summary>
		/// <param name="configuration">Configuration.</param>
		protected MineGameBase (GameConfiguration configuration) {
			Configuration = configuration;
		}

		/// <summary>
		/// Run this instance.
		/// </summary>
		public abstract void Run ();

		/// <summary>
		/// Pause this instance.
		/// </summary>
		public abstract void Pause ();

		/// <summary>
		/// Resume this instance.
		/// </summary>
		public abstract void Resume ();

		/// <summary>
		/// Happens before the initialization of the GLES context.
		/// </summary>
		public abstract void Prepare ();

		/// <summary>
		/// Update the logic here.
		/// Happens before OnRender.
		/// </summary>
		/// <param name="e">E.</param>
		public abstract void OnUpdate (UpdateEventArgs e);

		/// <summary>
		/// Render the scene here.
		/// Happens after OnUpdate.
		/// </summary>
		/// <param name="e">E.</param>
		public abstract void OnRender (UpdateEventArgs e);

		/// <summary>
		/// Performs application-defined tasks associated with freeing, releasing, or resetting unmanaged resources.
		/// </summary>
		/// <filterpriority>2</filterpriority>
		/// <remarks>Call <see cref="Dispose"/> when you are finished using the <see cref="Gimmes.MineEngine.MineGameBase"/>. The
		/// <see cref="Dispose"/> method leaves the <see cref="Gimmes.MineEngine.MineGameBase"/> in an unusable state. After
		/// calling <see cref="Dispose"/>, you must release all references to the <see cref="Gimmes.MineEngine.MineGameBase"/>
		/// so the garbage collector can reclaim the memory that the <see cref="Gimmes.MineEngine.MineGameBase"/> was occupying.</remarks>
		public virtual void Dispose () {
		}
	}
}

