﻿using System;

namespace Gimmes.MineEngine {

	/// <summary>
	/// Cross-Platform Runner.
	/// </summary>
	public static class Runner {

		public static TGame Run<TGame> (TGame game)
			where TGame : MineGameBase {
			game.Run ();
			return game;
		}
	}
}

