﻿using System;

namespace Gimmes.MineEngine {

	/// <summary>
	/// Shader.
	/// </summary>
	public interface IShader {

		/// <summary>
		/// Compile the shader.
		/// </summary>
		void Compile ();
	}
}

