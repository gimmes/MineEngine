﻿using System;

namespace Gimmes.MineEngine {

	/// <summary>
	/// Buffer.
	/// </summary>
	public interface IBuffer<T> : IBindable, IPointTo<T> {

		/// <summary>
		/// Gets or sets the size of the buffer.
		/// </summary>
		/// <value>The size of the buffer.</value>
		int BufferSize { get; set; }
	}
}

