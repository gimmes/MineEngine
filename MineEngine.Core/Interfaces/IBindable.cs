﻿using System;

namespace Gimmes.MineEngine {

	/// <summary>
	/// Bindable interface.
	/// </summary>
	public interface IBindable {

		/// <summary>
		/// Bind this instance.
		/// </summary>
		void Bind ();

		/// <summary>
		/// Unbind this instance.
		/// </summary>
		void Unbind ();
	}
}

