﻿using System;

namespace Gimmes.MineEngine {

	/// <summary>
	/// PointTo interface.
	/// </summary>
	public interface IPointTo<T> {
		
		void PointTo (T where);
		void PointTo (T where, T offset);
		void PointTo (T where, params T[] other);
	}
}

